# openrc

Dependency based service manager (runlevel change mechanism).  [openrc/openrc](https://github.com/openrc/openrc) and [wiki.gentoo.org/wiki/OpenRC](https://wiki.gentoo.org/wiki/OpenRC)

* [Distributions with OpenRC](https://distrowatch.com/search.php?defaultinit=OpenRC&status=Active)
